package com.cp.springreactive.Config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.cp.springreactive.Model")
@EnableJpaRepositories(basePackages = {
        "com.cp.springreactive.Repository" }, entityManagerFactoryRef = "mysqlEntityManager", transactionManagerRef = "mysqlTransactionManager")
public class MysqlDatasource {

    /**
     * Data source class name.
     */
    @Value("${mysql.datasource.driver-class-name}")
    private String className;

    /**
     * Data source URL.
     */
    @Value("${mysql.datasource.url}")
    private String url;

    /**
     * Data source URL.
     */
    @Value("${mysql.datasource.username}")
    private String userName;

    /**
     * Data source URL.
     */
    @Value("${mysql.datasource.password}")
    private String password;

    /**
     * Hibernate show sql.
     */
    @Value("${mysql.hibernate.show-sql}")
    private String showSql;

    /**
     * Format sql.
     */
    @Value("${mysql.hibernate.format_sql}")
    private String formatSql;

    /**
     * Hikari maximum pool size.
     */
    @Value("${spring.datasource.hikari.maximum-pool-size}")
    private int hikariMaxPoolSize;

    /**
     * Hikari minimum idle.
     */
    @Value("${spring.datasource.hikari.minimum-idle}")
    private int hikariMinIdle;

    /**
     * Hikari connection timeout.
     */
    @Value("${spring.datasource.hikari.connection-timeout}")
    private int hikariConnectionTimeOut;

    /**
     * Hikari idle timeout.
     */
    @Value("${spring.datasource.hikari.idle-timeout}")
    private int hikariIdleTimeout;

    /**
     * Hibernate Dialect.
     */
    @Value("${spring.hibernate-dialect}")
    private String springHibernateDialect;

    /**
     * Gets the mysql entity manager.
     *
     * @return {@link LocalContainerEntityManagerFactoryBean}, with the entity manager created.
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean mysqlEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(mysqlDataSource());
        em.setPackagesToScan("com.cp.springreactive.Model");
        em.setPersistenceUnitName("mysqlPU");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.dialect", this.springHibernateDialect);
        properties.put("hibernate.temp.use_jdbc_metadata_defaults", Boolean.FALSE);
        properties.put("hibernate.show_sql", this.showSql);
        properties.put("hibernate.format_sql", this.formatSql);
        em.setJpaPropertyMap(properties);
        return em;
    }

    /**
     * Gets the mysql data source.
     * 
     * @return {@link DataSource}, with the data source.
     */
    @Bean
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(this.className);
        dataSource.setUrl(this.url);
        dataSource.setUsername(this.userName);
        dataSource.setPassword(this.password);
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSource( dataSource);
        hikariConfig.setMaximumPoolSize(this.hikariMaxPoolSize);
        hikariConfig.setMinimumIdle(this.hikariMinIdle);
        hikariConfig.setConnectionTimeout(this.hikariConnectionTimeOut);
        hikariConfig.setIdleTimeout(this.hikariIdleTimeout);
        return new HikariDataSource(hikariConfig);
    }

    /**
     * Gets the mysql transaction manager.
     * 
     * @return {@link PlatformTransactionManager}, with the transaction manager.
     */
    @Bean
    public PlatformTransactionManager mysqlTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(mysqlEntityManager().getObject());
        transactionManager.setValidateExistingTransaction(true);
        return transactionManager;
    }

}

package com.cp.springreactive.Controller;

import com.cp.springreactive.Controller.InterfaceDefinitions.UserDefinition;
import com.cp.springreactive.Model.User;
import com.cp.springreactive.Services.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1")
public class UserController implements UserDefinition {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Flux<User> getUsers() {
        return this.userService.getAllUsers();
    }

    @Override
    public Mono<User> getUsers(Integer id) {
        return this.userService.getUserById(id);
    }
}

package com.cp.springreactive.Controller.InterfaceDefinitions;


import com.cp.springreactive.Model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserDefinition {

    @GetMapping("/user")
    Flux<User> getUsers();

    @GetMapping("/user/{id}")
    Mono<User> getUsers(@PathVariable Integer id);

}

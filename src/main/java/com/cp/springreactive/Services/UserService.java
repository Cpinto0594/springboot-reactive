package com.cp.springreactive.Services;

import com.cp.springreactive.Model.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<User> getUserById(Integer id);

    Flux<User> getAllUsers();

}

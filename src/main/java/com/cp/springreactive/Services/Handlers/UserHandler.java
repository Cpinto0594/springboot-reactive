package com.cp.springreactive.Services.Handlers;

import com.cp.springreactive.Model.User;
import com.cp.springreactive.Services.UserService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class UserHandler {

    private final UserService userService;

    public UserHandler(UserService userService) {
        this.userService = userService;
    }

    public Mono<ServerResponse> findUserByIdHandler(ServerRequest request) {
        return ServerResponse.ok()
                .body(userService.getUserById(Integer.parseInt(request.pathVariable("id"))), User.class)
                .onErrorResume(err -> ServerResponse.badRequest().build());
    }

    public Mono<ServerResponse> findUserAllUsersHandler(ServerRequest request) {
        return ServerResponse.ok()
                .body(userService.getAllUsers().collectList(), new ParameterizedTypeReference<>() {
                })
                .onErrorResume(err -> ServerResponse.badRequest().build());
    }

}

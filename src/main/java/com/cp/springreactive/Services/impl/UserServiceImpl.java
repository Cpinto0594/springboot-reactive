package com.cp.springreactive.Services.impl;

import com.cp.springreactive.Model.User;
import com.cp.springreactive.Repository.UserRepository;
import com.cp.springreactive.Services.UserService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Mono<User> getUserById(Integer id) {
        return Mono.justOrEmpty(this.userRepository.findById(id));
    }

    @Override
    public Flux<User> getAllUsers() {
        return Flux.fromIterable(this.userRepository.findAll());
    }
}

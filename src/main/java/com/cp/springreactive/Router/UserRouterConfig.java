package com.cp.springreactive.Router;

import com.cp.springreactive.Services.Handlers.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class UserRouterConfig {

    private final UserHandler userHandler;

    public UserRouterConfig(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    @Bean
    RouterFunction<ServerResponse> getEmployeeRoutes() {
        return route(GET("reactive/user/{id}"),
                userHandler::findUserByIdHandler
        ).and(route(GET("reactive/user"),
                userHandler::findUserAllUsersHandler));
    }

}
